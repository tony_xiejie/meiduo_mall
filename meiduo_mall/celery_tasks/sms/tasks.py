
from celery_tasks.sms.yuntongxun.sms import CCP#内层导包
# from meiduo_mall.libs.yuntongxun.sms import CCP #外层导包
# from celery_tasks.sms.yuntongxun import CCP 导包错误
from celery_tasks.sms import constans
from celery_tasks.main import celery_app

#任务必须经过装饰之后main.py才能识别任务代码
# 自定义任务名
@celery_app.task(name='sms_code')
def send_sms_code(mobile,sms_code):
    # CCP().send_template_sms(to='接受验证码的手机号',datas=[’短信验证码‘,’短信验证码的有效时间‘],temp_id=1（）)
    # CCP是云通讯第三方包的一个模块,专门用来向手机发短信的（短信验证码是后端自动生成的,CCP只负责发）
    CCP().send_template_sms(to=mobile,datas=[sms_code,constans.SMS_CODE_REDIS_EXPIRES//60],temp_id=1)