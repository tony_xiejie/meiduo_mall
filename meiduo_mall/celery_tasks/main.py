#celery客户端创建以及celery程序启动
from celery import Celery
import os

# 在当前程序系统环境中添加django配置文件模板
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "meiduo_mall.settings.dev")
# 1.创建celery客户端对象,形参是程序别名
celery_app = Celery('meiduo')

# 2.加载配置信息（指定谁来当任务队列,作用与仓库类似）
celery_app.config_from_object('celery_tasks.config')

# 3.指定celery生产任务（任务不止一个，所以要使用列表）
# celery的版本V4.3.0 redis的版本V3.2.0
celery_app.autodiscover_tasks(['celery_tasks.sms','celery_tasks.email'])