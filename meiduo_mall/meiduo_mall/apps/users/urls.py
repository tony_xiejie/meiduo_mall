from django.conf.urls import url
from . import views

urlpatterns = [
    # 注册页面的展示
    url(r'^register/$',views.RegisterView.as_view(),name='register'),
    # 登陆页面的展示
    url(r'^login/$',views.LoginView.as_view(),name='login'),

    url(r'^usernames/(?P<username>[a-zA-Z0-9_-]{5,20})/count/$', views.UsernameCountView.as_view()),

    # 手机号是否重复
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),

    # 用户退出登陆
    url(r'^logout/$',views.LogoutView.as_view(),name='logout'),
    # 用户中心页面的展示
    url(r'^info/$',views.InfoView.as_view(),name='userinfo'),
    # 用户邮箱
    url(r'^emails/$', views.EmailView.as_view()),
    # 用户邮箱激活
    url(r'^emails/verification/$', views.EmailVerifyView.as_view()),
    # 添加收货地址
    url(r'^addresses/$', views.AddressView.as_view()),
    # 添加收货地址
    url(r'^addresses/create/$', views.AddressCreateView.as_view()),
    # 用户商品浏览记录
    url(r'^browse_histories/$', views.UserBrowseHistory.as_view()),
]