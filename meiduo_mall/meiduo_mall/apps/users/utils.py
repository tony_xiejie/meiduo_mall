from django.contrib.auth.backends import ModelBackend
import re
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadData
from django.conf  import settings
from .models import User

# Django自带了用户认证系统,改写的目的就是为多账户登陆
# 用户名和手机号都可以登陆
def get_user_account(account):
    """
    通过用户名或手机号查询user
    :param account: 用户名 or 手机号
    :return: user or None
    """
    # 判断用户名一栏输入的是否是纯数字
    try:
        if re.match(r'^1[3-9]\d{9}$', account):
            user = User.objects.get(mobile=account)
        else:
            user = User.objects.get(username=account)
        return user
    except User.DoesNotExist:
        return None


class UsernameMobileAuthBackend(ModelBackend):
    """自定义用户认证后端类"""
    def authenticate(self, request, username=None, password=None, **kwargs):
        # 1.动态的根据用户名或手机号查询用户
        user = get_user_account(username)
        # 2.校验用户密码
        if user and user.check_password(password):
            # 返回user
            return user

def generate_email_verify_url(user):
    """
    生成邮箱激活url
    user: 要生成激活url 用户
    :return: 拼接好的邮件激活url
    """
    # 创建加密对象
    serializer = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    # 包装加密数据
    data = {'user_id': user.id, 'email': user.email}
    # dumps
    token = serializer.dumps(data).decode()
    # 'http://www.meiduo.site:8000/emails/verification/' + '?token=' + token
    # 拼接激活url
    verify_url = settings.EMAIL_VERIFY_URL + '?token=' + token
    return verify_url


def check_email_user(token):
    """
    对用户激活url后面的数据进行解密,并查询user
    :param token: 用户加密信息
    :return: user or None
    """
    # 创建加密/解密对象
    serializer = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)
    # loads方法解密
    try:
        data = serializer.loads(token)
        user_id = data.get('user_id')
        email = data.get('email')
        try:
            user = User.objects.get(id=user_id, email=email)
            return user
        except User.DoesNotExist:
            return None
    except BadData:
        return None
