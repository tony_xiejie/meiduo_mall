from django.shortcuts import render
from django.views import View
from django import http
from django.core.cache import cache

from .models import Area
from meiduo_mall.utils.response_code import RETCODE
# Create your views here.
# 查询所有省: 没有查询参数
# Area.objects.filter(parent=None)
# 查询指定省下面的所有市: 有查询参数 : 省id  130000
# Area.objects.filter(parent_id=130000)
# 查询指定市下面的所有区: 有查询参数:  市id: 130400
# Area.objects.filter(parent_id=130400)

# get   /areas/
# print(Area.objects.filter(parent=None)) 测试是否后端能取得数据库数据
# json数据不能包装查询集数据或对象，只能包装元组，字符串，列表等基础数据
class AreaView(View):
    """省市区查询"""
    def get(self, request):
        # 1.接收查询参数
        area_id = request.GET.get('area_id')
        # 2.判断是否有查询参数
        if area_id is None:

            # 优先从缓存中获取所有省数据
            province_list = cache.get('province_list')
            # 判断是否获取到所有省数据,如果没有,再从mysql中查询
            if province_list is None:
                # 3.如果没有查询参数,代表用户想获取所有省数据
                # 查询所有省数据,返回所有省查询集
                province_qs = Area.objects.filter(parent=None)
                # 使用ORM时从数据库查询出来的数据响应要利用json响应给前端之前,都要进行模型转字典(序列化)
                # 定义一个列表用来包装所有省字典数据
                province_list = []
                # 遍历province_qs 将查询集中的模型转换成字典
                for province_model in province_qs:
                    province_list.append({
                        'id': province_model.id,
                        'name': province_model.name
                    })
                # 存储所有省数据到缓存中
                # cache.set('key', 'value', '过期时间(秒)')
                cache.set('province_list', province_list, 3600)
            return http.JsonResponse({'code': RETCODE.OK, 'errmsg': 'ok', 'province_list': province_list})
        else:

            # 优先从缓存中获取所有数据
            sub_data = cache.get('sub_data_%s' % area_id)
            # 如果缓存中没有获取到数据,就从mysql查询
            if sub_data is None:
                # 如果有查询参数,代表查询指定id的所有下级行政区数据
                try:
                    # 根据area_id 将指定上级行政区查询出来
                    parent_model = Area.objects.get(id=area_id)
                    # 通过上级查询下级所有行政区
                    sub_qs = parent_model.subs.all()
                except Area.DoesNotExist:
                    return http.HttpResponseForbidden('area_id不存在')

                # 模型转字典
                # 定义用来包装所有下级行政区的列表
                sub_list = []
                # 遍历sub_qs 下级行政区查询
                for sub_model in sub_qs:
                    sub_list.append({
                        'id': sub_model.id,
                        'name': sub_model.name
                    })

                # 包装上级及下级所有数据
                sub_data = {
                    'id': parent_model.id,
                    'name': parent_model.name,
                    'subs': sub_list
                }

                # 将mysql查询出来的数据存储到缓存中，默认存储到0号redis数据库中
                cache.set('sub_data_%s' % area_id, sub_data, 3600)
            # 响应
            return http.JsonResponse({'code': RETCODE.OK, 'errmsg': 'OK', 'sub_data': sub_data})


