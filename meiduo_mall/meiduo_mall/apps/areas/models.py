from django.db import models

# class Area(models.Model):
#     """省"""
#     # area_set = ''
#     subs = ''

class Area(models.Model):
    """市"""

    name = models.CharField(max_length=20, verbose_name='名称')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='subs', null=True, blank=True, verbose_name='上级行政区划')

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '省市区'
        verbose_name_plural = '省市区'

    def __str__(self):
        return self.name


# Area.objects.filter(parent=None)  # 所有省
# Area.objects.filter(parent__isnull=True)  # 所有省

# 市的parent_id 指向的就是上级省
# 河北省: 130000
# Area.objects.filter(parent_id=130000)  # 指定省下面的所有市
# Area.objects.filter(parent=hbsModel)  # 指定省下面的所有市
# Area.objects.filter(parent=citymodel)  # 指定市下面的所有区
# hbs.subs.all()  # 一查多
# szs.subs.all()  # 一查多


# szmodel.parent   # 通过深圳市查询上级省



# class BookInfo():  # 一
#     pass
#     # heroinfo_set = ''
#     subs = ''
#
# class HeroInfo:   # 多
#     pass
#     hbook = ''


# book.heroinfo_set.all()
# book.subs.all()

# hero.hbook