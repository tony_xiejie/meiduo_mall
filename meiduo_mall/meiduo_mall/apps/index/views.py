from django.shortcuts import render

# Create your views here.
from django.views import View
from .utils import get_categories,test
from .models import Content, ContentCategory

class IndexView(View):

    def get(self,request):
        """
        {
            'index_lb': ['lb'], # 一个列表代表同一类型的所有广告
            'index_kx': ['kx']
        }
        """

        # 1.定义用来包装所有广告数据的大字典
        contents = {}
        # 2. 将所有广告类别数据全部获取到
        content_cat_qs = ContentCategory.objects.all()
        # 3. 遍历广告类别模型
        for content_cat_model in content_cat_qs:
            # 4.包装广告数据
            contents[content_cat_model.key] = content_cat_model.content_set.filter(status=True).order_by('sequence')

        content = {
            'categories':get_categories(),# 商品类别说明
            # 'categories':test(),# 商品类别说明
             'contents': contents  # 首页所有广告数据
        }

        return render(request, 'index.html', content)