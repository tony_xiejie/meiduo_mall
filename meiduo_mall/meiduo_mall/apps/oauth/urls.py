from django.conf.urls import url

from . import views


urlpatterns = [
    # 拼接QQ登录url
    url(r'^qq/authorization/$', views.QQOAuthURLView.as_view()),
    # QQ登录回调处理
    url(r'^oauth_callback$', views.QQOAuthUserView.as_view()),
]