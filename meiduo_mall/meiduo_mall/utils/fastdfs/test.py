# 1. 导入FastDFS客户端扩展
from fdfs_client.client import Fdfs_client
# 2. 创建FastDFS客户端实例
client = Fdfs_client('client.conf')
# 3. 调用FastDFS客户端上传文件方法
ret = client.upload_by_filename('cat.jpg')

print(ret)

# {'Group name': 'group1',
#  'Remote file_id': 'group1/M00/00/00/wKjcgF52xwuAEfXJAAAQ7colEzw519.jpg',
#  'Status': 'Upload successed.',
#  'Local file name': 'cat.jpg',
#  'Uploaded size': '4.00KB',
#  'Storage IP': '192.168.220.128'}