"""meiduo_mall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # 首页模块
    url(r'^',include('index.urls')),# 首页模块
    url(r'^',include('verifications.urls')),# 验证码模块
    url(r'^', include('users.urls',namespace='users')),#用户模块
    url(r'^', include('oauth.urls')),  # QQ模块
    url(r'^', include('goods.urls')),  # 商品信息模块
    url(r'^', include('areas.urls')),  # 商品收货地址模块
    url(r'^', include('carts.urls')),  # 购物车模块
    url(r'^', include('orders.urls')),  # 订单模块
    url(r'^', include('payment.urls')),  # 支付模块
]
